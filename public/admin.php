<?php

/**
 * Задача 6. Реализовать вход администратора с использованием
 * HTTP-авторизации для просмотра и удаления результатов.
 pass:1123;
 **/

// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.
$user = 'u35658';
$pass = '3463246';
$db = new PDO('mysql:host=localhost;dbname=u35658', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
try{
  $stmt = $db->prepare("SELECT * FROM `admin`");
  $stmt->execute();
  $result = $stmt->fetch(PDO::FETCH_NUM);
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
}
if (empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW']) || $_SERVER['PHP_AUTH_USER'] != $result[0] || $_SERVER['PHP_AUTH_PW'] != $result[1])
{ 
  header('HTTP/1.1 401 Unanthorized');
  header('WWW-Authenticate: Basic realm="My site"');
  print('<h1>401 Требуется авторизация</h1>');
  exit();
}

print('Вы успешно авторизовались и видите защищенные паролем данные.');

session_start();
// *********
// Здесь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
// Реализовать просмотр и удаление всех данных.
// *********
?>
<?php
     if ($_SERVER['REQUEST_METHOD'] == 'POST')
	 {
     	$_SESSION['uid'] = $_POST['id'];
     	header('Location: admin_index.php');
     }
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Admin</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <link rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
</body>
</html>
<body>

	<table>
		<tr>
			<td>Бессмертных</td>
			<td>Проходящих сквозь стены</td>
			<td>Левитирующих</td>
		</tr>
		<tr>
<?php
			$user = 'u35658';
			$pass = '3463246';
			$db = new PDO('mysql:host=localhost;dbname=u35658', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
			try{
			  for ($i=1; $i <= 3 ; $i++) { 
				$stmt = $db->prepare("SELECT COUNT(*) FROM `spwids` WHERE `spwid` = ?");
			    $stmt->execute(array($i));
			    $result = $stmt->fetch(PDO::FETCH_NUM);
			    print("<td>" . $result[0] . "</td>");
			  }
			}
			catch(PDOException $e){
			  print('Error : ' . $e->getMessage());
			}
?>
		</tr>
	</table>

<?php
    		$user = 'u35658';
    		$pass = '3463246';
    		$db = new PDO('mysql:host=localhost;dbname=u35658', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    		try{
    		    $stmt = $db->prepare("SELECT * FROM `users`");
    		    $stmt->execute();
    		    $result = $stmt->fetchAll(PDO::FETCH_NUM);
    		}
    		catch(PDOException $e){
    		  print('Error : ' . $e->getMessage());
    		}
?>


	<table>
	<tr>
	    <td>ID</td>
	    <td>Имя</td>
	    <td>Email</td>
	    <td>Год рождения</td>
	    <td>Пол</td>
	    <td>Количество конечностей</td>
	    <td>Биография</td>
	    <td>Логин</td>
	    <td>Хэш пароля</td>
	    <td>Суперспособности</td>
	</tr>
<?php
     for($i=0; $i<count($result); $i++){
    	print("<tr> ");
    	for($j = 0; $j<count($result[0]); $j++){
    		print("<td>" . $result[$i][$j] . "</td>");
    	}
    	$stmt = $db->prepare("SELECT `naveofspw` FROM `spwdescriptor` WHERE spwid = ANY(SELECT `spwid` FROM `spwids` WHERE id = ?)");
	    $stmt->execute(array($result[$i][0]));
	    $sp_array = $stmt->fetchAll(PDO::FETCH_NUM);
    	print("<td>");
    	if(!empty($sp_array)){
	    	for ($k=0; $k < count($sp_array); $k++) { 
	    		print($sp_array[$k][0] . "  ");
	    	}
    	}
    	print("</td>");
    	print("</tr>");
     }
?>
    </table>
     <form method="POST" action="admin.php">
        <input type="text" name="id" placeholder="Введите id">
        <input type="submit" name="Изменить">
     </form>
</body>
